def compareJson(example_json_s, target_json_s):
 example_json = json.loads(example_json_s)
 target_json = json.loads(target_json_s)
 return compareParsedJson(example_json, target_json)

def compareParsedJson(example_json, target_json):   
 for x in example_json:
   if type(example_json[x]) is not dict:
     if not x in target_json or not example_json[x] == target_json[x]:
       return False
   else:
     if x not in target_json or not compareParsedJson(example_json[x], target_json[x]):
      return False

 return True
