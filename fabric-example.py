from fabric.api import *
import os,sys

def runcmd(mycmd, myhost):
    with hide('output','running','warnings'):
        env.host_string = myhost
        env.timeout = 2 
        env.user = 'admin'
        return sudo(mycmd)

my_instances = ['172.22.45.189', '172.22.45.155', '172.22.1.100', '172.22.1.86', '172.22.1.75', '172.22.1.31', '172.22.45.136', '172.22.45.5']

i=0
for my_instance in my_instances:
   i+=1
   print "running on " + my_instance
   try:
      print "Will run on  the " + str(i) + " " + my_instance 
      output2 = runcmd("ls", my_instance)
      print "Successfully ran."
   except:
      the_type, the_value, the_traceback = sys.exc_info()
      print "Exception: " + str(the_type) + "Traceback: " + str(the_traceback)
      continue
