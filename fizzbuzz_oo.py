class FizzBuzz(object):

    def start(self, end_number):
        return ",".join(self._parse_numbers(end_number))

    def _parse_numbers(self, end_number):
        number_list = []
        for number in range(1, end_number+1):
            number_list.append(self.parse_number(number))
        return number_list

    def parse_number(self, number):
        if self._is_number_divisible_by_five_and_three(number):
            return "FizzBuzz"
        elif self._is_number_divisible_by_three(number):
            return "Fizz"
        elif self._is_number_divisible_by_five(number):
            return "Buzz"
        else:
            return str(number)

    def _is_number_divisible_by_five_and_three(self, number):
        return number % 15 == 0

    def _is_number_divisible_by_three(self, number):
        return number % 3 == 0

    def _is_number_divisible_by_five(self, number):
        return number % 5 == 0

fb = FizzBuzz()
print fb.start(133)
