#!/usr/bin/env python
import tornado.ioloop
import maproxy.proxyserver

server = maproxy.proxyserver.ProxyServer("www.pinterest.com",443,server_ssl_options=True)
server.listen(8888)
print("requests to http://127.0.0.1:8888 are forwarded to https://www.pinterest.com",)
tornado.ioloop.IOLoop.instance().start()
