from twisted.internet import reactor
from twisted.web import proxy, server

site = server.Site(proxy.ReverseProxyResource('archive.org', 80, b''))
reactor.listenTCP(8081, site)
reactor.run()
