from twisted.internet import reactor, endpoints
from twisted.web import proxy, server

site = server.Site(proxy.ReverseProxyResource('www.pinterest.com', 443, ''))
endpoint = endpoints.TCP4ServerEndpoint(reactor, 8080)
endpoint.listen(site)
reactor.run()
