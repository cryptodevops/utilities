from twisted.web import server, resource
from twisted.internet import reactor
import urllib2,pprint

class Simple(resource.Resource):
    isLeaf = True
    def render_GET(self, request):
        my_url = 'https://www.pinterest.com'
        request = urllib2.Request(my_url)
        result = urllib2.urlopen(request)
        page_source = result.read()
        print page_source 
        #return b"<html>Hello, world!</html>"
        #return map(bin,bytearray(page_source))
        return page_source.encode('ascii') 

my_url = 'https://www.pinterest.com'
request = urllib2.Request(my_url)
result = urllib2.urlopen(request)
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(result)
site = server.Site(Simple())
reactor.listenTCP(8088, site)
reactor.run()
